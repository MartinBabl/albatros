%Longitudenal design
Cxt = -CDt*cos(alphaT)+CLt*sin(alphaT);
Along = [rho*velocityT*S*Cxt/m qt*S*(Cla*alphaT+CLt-(2*CLt*Cla/(pi*A*e)))/m 0 -g*cos(alphaT);
      -rho*S*CLt/m -qt*S*Cla/(m*velocityT) 1-qt*S*c_bar*Clq/(m*velocityT*2*velocityT) -g*sin(alphaT)/velocityT;
      0 qt*S*c_bar*Cma/(I_B(2)) qt*S*c_bar*c_bar*Cmq/(I_B(2)*2*velocityT) 0;
      0 0 1 0];
%[V,D] = eig(Along);
BdeltaE = [0;
           qt*S*Clde/(m*velocityT);
           -qt*S*c_bar*Cmde/(I_B(2));
           0];%Multipiled by minus one 
Cq = [0 0 1 0];

s = tf('s');
sstrf = Cq*(((s*eye(4)-Along))\BdeltaE);
%rltool(sstrf) %Kq = -0.023681
Kq = 0.023681;
Blong = [0 1/m;
           qt*S*Clde/(m*velocityT) 0;
           -qt*S*c_bar*Cmde/(I_B(2)) 0;
           0 0];
APRD = [Along - Blong*[Kq; 0]*Cq];
CV_barHdot = [1 0 0 0;
              0 -velocityT 0 velocityT];

AascrD = [Along [0 0; 0 0; 0 0; 0 0];
         CV_barHdot [0 0; 0 0]];
BascrD = [Blong; [0 0; 0 0]];

maxV_bar = 140;
maxAlpha = 15;
maxPitchRate = 5;
maxTheta = 40;
maxVerr = 40;
maxHerr = 1;

maxDeltaE = 30;
maxDeltaT = 1;

Qascr = diag([1/((maxV_bar)^2) 1/((maxAlpha)^2) 1/((maxPitchRate)^2) 1/((maxTheta)^2) 1/((maxVerr)^2) 1/((maxHerr)^2)]);

Rascr = diag([1/(maxDeltaE^2) 1/(maxDeltaT^2)]);

Kascr = lqr(AascrD,BascrD,Qascr,Rascr);

Aascr = AascrD - BascrD*Kascr;
Bascr = [0 0; 0 0; 0 0; 0 0; -1 0; 0 -1];
Bcr = [0;0;0;0;0;-1];
CHdot = [0 -velocityT 0 velocityT 0 0]; 

altCon = 1/s*CHdot*((s*eye(6)-Aascr)\Bcr);

%rltool(altCon); 
Kh = 0.67669;

%Latitudenal design

Alat = [qt*S*Cyb/(m*velocityT) qt*S*b*Cyp/(m*velocityT*2*velocityT) ((qt*S*Cyr/(m*velocityT))-1) g*cos(alphaT)/(velocityT);
        qt*S*b*Clb/(I_B(1)) qt*S*b*b*Clp/(I_B(1)*2*velocityT) qt*S*b*b*Clr/(I_B(1)*2*velocityT) 0;
        qt*S*b*Cnb/(I_B(3)) qt*S*b*b*Cnp/(I_B(3)*2*velocityT) qt*S*b*b*Cnr/(I_B(3)*2*velocityT) 0;
        0 1 tan(alphaT) 0];

Blat = [qt*S*Cyda/(m*velocityT) qt*S*Cydr/(m*velocityT);
        qt*S*b*Clda/(I_B(1)) qt*S*b*Cldr/(I_B(1));
        qt*S*b*Cnda/(I_B(3)) qt*S*b*Cndr/(I_B(3));
        0 0];
    
BdeltaR = [qt*S*Cydr/(m*velocityT);
           qt*S*b*Cldr/(I_B(1));
           qt*S*b*Cndr/(I_B(3));
           0];

          

 Cr = [0 0 1 0];
 s = tf('s');
 ssTrfDRD = Cr*((s*eye(4)-Alat)\BdeltaR);
 %rltool(ssTrfDRD);
 %Dutch roll mode natural frequency = 8.34rad/s
 Kr = -0.175;
 %Washout filter cutoff frequency of 2rad/s
 tw = 2;
 Aw = -1/tw;
 Bw = Kr;
 Cw = -1/tw;
 Dw = Kr;
 
 Adrd = [(Alat-(BdeltaR*Dw*Cr) ) -BdeltaR*Cw;
         Bw*Cr Aw];
 Bdrd = [BdeltaR;
         0 ];
 Cdrd = [0 0 1 0 0];
 %ssTrfDRDwf = Cdrd*((s*eye(5)-Adrd)\-Bdrd);
 %rltool(ssTrfDRDwf);
 
 Croll = [0 0 0 1 0];
 
 BdeltaA = [qt*S*Cyda/(m*velocityT);
           qt*S*b*Clda/(I_B(1));
           qt*S*b*Cnda/(I_B(3));
           0;
           0]; 
 %sstrfRAC = Croll*inv(s*eye(5)-Adrd)*BdeltaA;
 
 %rltool(sstrfRAC);
 Kp=0.01;
 Ki=1;
 Arac = [Adrd+BdeltaA*Kp*Croll -BdeltaA*Ki;
         -Croll 0];
 Brac = [-Kp*BdeltaA;
           1];
 Crac = [0 0 0 1 0 0];
 %sstrfRAC = Crac*inv(s*eye(6)-Arac)*Brac
 
 %rltool(sstrfRAC);
 
 