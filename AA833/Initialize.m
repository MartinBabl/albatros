%Inertia
m = 0.7;%Mass kg
I_B = [0.2 0.36 0.525]; %Ixx, Iyy, Izz
rho = 1.225;%Air densisty coeffiecient
tau = 0.25;%Engine time constant
g = 9.81;%Acceleration due to gravity

%Geometry
A = 4.1666;
c_bar = 0.15;
b = 1.73;
S = 0.3;
GeoCoe = [A c_bar b S];

%Aerodynamic coefficients
Cd0 = 0.0300;
e = 0.85;

%-Lift
Cl0 = 0.0;
Cla = 5.1309;
Clq = 7.7330;
Clde = 0.7126;
Clift = [Cl0, Cla, Clq, Clde];

%-Pitch moment
Cm0 = 0.0;
Cma = -0.2954;
Cmq = -10.281;
Cmde = -1.5852;
Cm = [Cm0, Cma, Cmq, Cmde];

%-Side force
Cyb = -0.2777;
Cyp = 0.0102;
Cyr = 0.2122;
Cyda = -0.0077;
Cydr = 0.2303;
Cy = [Cyb, Cyp, Cyr, Cyda, Cydr];

%-Roll
Clb = -0.0331;
Clp = -0.4248;
Clr = 0.045;
Clda = -0.3731;
Cldr = 0.0080;
Cl = [Clb, Clp, Clr, Clda, Cldr];


%-Yaw moment
Cnb = 0.0860;
Cnp = -0.0251;
Cnr = -0.1250;
Cnda = -0.0065;
Cndr = -0.1129;
Cn = [Cnb, Cnp, Cnr, Cnda, Cndr];

%Initial Conditions
TrimCalculations;
Wi = [0.0 0.0 0.0];%Angular Rates
Pi = [0.0 0.0 -10000.0];%Position
Ei = [0.0 -1.575 0.0];%Orientation-Euler
V_barRef = 18; 
HeadingRef = 0.3;%rads
Modes;