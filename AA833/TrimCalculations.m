%TRIMCALCULATIONS Summary of this function goes here
%   Detailed explanation goes here
velocityT = 18.0; %m/s
FlightPathAngle = 0.8244;
rho = 1.225;
qt = 0.5*rho*velocityT^2;
calc = ([Cla Clde; Cma Cmde])\[(m*g/qt/S-Cl0); -Cm0];
alphaT = calc(1)
deltaEt = calc(2)
CLt = m*g/(qt*S)/cos(FlightPathAngle);
CDt = Cd0 + CLt^2/(pi*A*e);
ThrustT = qt*S*CDt*cos(alphaT)-qt*S*CLt*sin(alphaT)+m*g*sin(alphaT)
Vi = [35.0 0.0 0.0]